# Tarsier-Internship

#### Description
```
This project is used to record the practice process in Tarsier and the output in the process
```

#### Directory description
```
Report  Log and Report
    -> log.  record the work process
    -> output. record the output of the summary document
```
