
# 【2022/10/8 记录】

```bash
一、上次遗留工作跟踪
  尝试修包：https://build.openeuler.org/package/show/openEuler:Mainline:RISC-V/libappstream-glib
二、工作及进展
  熟悉rpm包结构及制作，还未开始libappstream-glib修包工作
三、新增工作
  VisionFive v1 openeuler系统中 wifi驱动添加
```




# 【2022/9/15 记录】

```bash
一、上次遗留工作跟踪
  无
二、工作及进展
  搭建qemu-riscv+openeuler开发环境，完成
  搭建visionfive+openeuler开发环境，完成
三、新增工作
  尝试修包：https://build.openeuler.org/package/show/openEuler:Mainline:RISC-V/libappstream-glib
```

