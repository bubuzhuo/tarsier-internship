# Tarsier-Internship

## 介绍
```
本工程是用来记录在 Tarsier 的实习过程以及过程中的输出
```

## 目录说明
```
Report  日志与报告，记录工作过程及输出成果的总结性文档
    -> log  记录工作过程
    -> output 输出成果的总结性文档
```